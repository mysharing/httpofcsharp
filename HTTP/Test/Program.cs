﻿using Newtonsoft.Json.Linq;
using Opc.Ua;
using PengMingXing.NetCommunicationCommons.HTTP.HttpClientEx;
using PengMingXing.NetCommunicationCommons.HTTP.HttpWebRequestEx;
using PengMingXing.NetCommunicationCommons.OPC.OPCUAEx;

namespace Test
{
    internal class Program
    {
        /// <summary>
        /// HttpWebRequest Demo
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            //Instantiate the OPCUA object
            //OPCUAClientHelper oPCUAClientHelper = new OPCUAClientHelper("opc.tcp://127.0.0.1:49320");
            ////Write data fetch events
            //oPCUAClientHelper.OnTagValueReceiveEvent += OPCUAClientHelper_OnTagValueReceiveEvent;
            ////Create a dictionary get node
            //Dictionary<string, Type> keyValuePairs = new Dictionary<string, Type>();
            //keyValuePairs.Add("ns=2;s=通道 1.1#chu.1#c_1juanqujcd", typeof(bool));
            //keyValuePairs.Add("ns=2;s=通道 1.1#chu.1#c_1houdu", typeof(float));
            ////Connect to the KepServerex server
            //await oPCUAClientHelper.ConnectServer(keyValuePairs);
            //////..............
            //oPCUAClientHelper.SetServerNodeValue("ns=2;s=通道 1.1#chu.1#c_1juanqujcd", true);
            //oPCUAClientHelper.SetServerNodeValue<bool>("ns=2;s=通道 1.1#chu.1#c_1juanqujcd", false);
            //await oPCUAClientHelper.SetServerNodeValueAsync<bool>("ns=2;s=通道 1.1#chu.1#c_1juanqujcd", true);

            HttpClientHelper httpClientHelper = new HttpClientHelper();
            await httpClientHelper.UploadFilesAsync("https://localhost:7022/api/ImageWatermark/AddWatermark", @"C:\Users\user\Desktop\123.jpg", "测试", PengMingXing.NetCommunicationCommons.ContentTypeEnum.Multipart_Form_Data);

            //Close the OPCUA client
            //oPCUAClientHelper.DisConnectServer();
            while (true)
            {
                Thread.Sleep(1000);
            }
        }
        /// <summary>
        /// Continuously obtain the corresponding node data from the server for use
        /// </summary>
        /// <param name="dr"></param>
        private static void OPCUAClientHelper_OnTagValueReceiveEvent(System.Data.DataRow dr)
        {
            Console.WriteLine(dr["ns=2;s=通道 1.1#chu.1#c_1juanqujcd"]);
            Console.WriteLine(dr["ns=2;s=通道 1.1#chu.1#c_1houdu"]);
        }
    }
}