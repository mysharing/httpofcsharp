﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons
{
    /// <summary>
    ///  常见的 Content-Type
    /// </summary>
    public enum ContentTypeEnum
    {
        //text开头
        Text_Html,//text/html：HTML格式
        Text_Plain,//text/plain：纯文本格式
        Text_Xml,//text/xml： XML格式
        //图片格式
        Image_Gif,//image/gif ：gif 图片格式
        Image_Jpeg,//image/jpeg ：jpg 图片格式
        Image_Png,//image/png：png 图片格式
        //application开头
        Application_Xhtml_Xml,//application/xhtml+xml：XHTML 格式
        Application_Xml,//application/xml：XML 数据格式
        Application_Atom_Xml,//application/atom+xml：Atom XML 聚合格式
        Application_Json,//application/json：JSON 数据格式
        Application_Pdf,//application/pdf：pdf 格式
        Application_Msword,//application/msword：Word 文档格式
        Application_Octet_stream,//application/octet-stream：二进制流数据（如常见的文件下载）
        Application_X_WWW_Urlencoded,//application/x-www-form-urlencoded：表单发送默认格式
        //媒体文件
        Audio_X_WAV,//audio/x-wav：wav文件
        Audio_X_MS_WMA,//audio/x-ms-wma：w文件
        Audio_MP3,//audio/mp3：mp3文件
        Video_X_MS_WMV,//video/x-ms-wmv：wmv文件
        Video_Mpeg4,//video/mpeg4：mp4文件
        Video_AVI,//video/avi：avi文件
        Multipart_Form_Data//multipart/form-data
    }
}
