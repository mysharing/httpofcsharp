﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons
{

    /*
    一、C# 中发送 http 请求的方式：
    ① HttpWebRequest:
    .Net 平台原生提供，这是 .Net 创建者最初开发用于使用 HTTP 请求的标准类，使用 HttpWebRequest 可以让
    开发者控制请求/响应流程各个方面。
    ② WebClient：
    .Net 平台原生提供，WebClient 是一种更高级别的抽象， 是 HttpWebRequest 为了简化最常见任务而创建的，
    但也因此缺少了 HttpWebRequest 的灵活性
    ③ HttpClient：
    .Net 平台原生提供
    ④ RestSharp:
    开源项目，基于 HttpWebRequest 做的二次封装
    ⑤ Flurl:
    开源项目，基于 HttpClient 做的二次开发， 项目地址：https://github.com/tmenier/Flurl

    但是现在 .Net Core 平台下更推荐使用 HttpClient，再此处做个记录，学习

    二、HttpClient 
    基于 Socket 开发
     
     */
    internal static class CommunicationHelper
    {
        public static string ContentTypeConvrt(this ContentTypeEnum contentType)
        {
            string result = string.Empty;
            switch (contentType)
            {
                case ContentTypeEnum.Text_Html://text/html：HTML格式
                    result = "text/html";
                    break;
                case ContentTypeEnum.Text_Plain://text/plain：纯文本格式
                    result = "text/plain";
                    break;
                case ContentTypeEnum.Text_Xml://text/xml： XML格式
                    result = "text/xml";
                    break;
                case ContentTypeEnum.Image_Gif://image/gif ：gif 图片格式
                    result = "image/gif";
                    break;
                case ContentTypeEnum.Image_Jpeg://image/jpeg ：jpg 图片格式
                    result = "image/jpeg";
                    break;
                case ContentTypeEnum.Image_Png://image/png：png 图片格式
                    result = "image/png";
                    break;
                case ContentTypeEnum.Application_Xhtml_Xml://application/xhtml+xml：XHTML 格式
                    result = "application/xhtml+xml";
                    break;
                case ContentTypeEnum.Application_Xml://application/xml：XML 数据格式
                    result = "application/xml";
                    break;
                case ContentTypeEnum.Application_Atom_Xml://application/atom+xml：Atom XML 聚合格式
                    result = "application/atom+xml";
                    break;
                case ContentTypeEnum.Application_Json://application/json：JSON 数据格式
                    result = "application/json";
                    break;
                case ContentTypeEnum.Application_Pdf://application/pdf：pdf 格式
                    result = "application/pdf";
                    break;
                case ContentTypeEnum.Application_Msword://application/msword：Word 文档格式
                    result = "application/msword";
                    break;
                case ContentTypeEnum.Application_Octet_stream://application/octet-stream：二进制流数据（如常见的文件下载）
                    result = "application/octet-stream";
                    break;
                case ContentTypeEnum.Application_X_WWW_Urlencoded://application/x-www-form-urlencoded：表单发送默认格式
                    result = "application/x-www-form-urlencoded";
                    break;
                case ContentTypeEnum.Audio_X_WAV://audio/x-wav：wav文件
                    result = "audio/x-wav";
                    break;
                case ContentTypeEnum.Audio_X_MS_WMA://audio/x-ms-wma：w文件
                    result = "audio/x-ms-wma";
                    break;
                case ContentTypeEnum.Audio_MP3://audio/mp3：mp3文件
                    result = "audio/mp3";
                    break;
                case ContentTypeEnum.Video_X_MS_WMV://video/x-ms-wmv：wmv文件
                    result = "video/x-ms-wmv";
                    break;
                case ContentTypeEnum.Video_Mpeg4://video/mpeg4：mp4文件
                    result = "video/mpeg4";
                    break;
                case ContentTypeEnum.Video_AVI://video/avi：avi文件
                    result = "video/avi";
                    break;
                case ContentTypeEnum.Multipart_Form_Data:
                    result = "multipart/form-data";
                    break;
            }
            return result;
        }
    }
}

///许可证示例
//< Project Sdk = "Microsoft.NET.Sdk" >

//  < PropertyGroup >
//    < TargetFramework > netstandard2.0 </ TargetFramework >
//    < PackageLicenseExpression > MIT OR Apache-2.0</PackageLicenseExpression>
//  </PropertyGroup>
//</Project>
