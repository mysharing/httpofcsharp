﻿using Microsoft.Extensions.DependencyInjection;
using PengMingXing.NetCommunicationCommons.HTTP.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.HTTP.HttpClientEx
{
    public static class HttpClientFactoryHelper
    {
        /// <summary>
        /// Create HttpClient
        /// </summary>
        /// <returns></returns>
        public static HttpClient CreateHttpClient() 
        {
            //创建 服务集合
            ServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddHttpClient();
            return ConfigurationClient(serviceCollection);
        }

        /// <summary>
        /// Create HttpClient
        /// </summary>
        /// <param name="ClientKey"></param>
        /// <returns></returns>
        public static HttpClient CreateHttpClient(string ClientKey)
        {
            //创建 服务集合
            ServiceCollection serviceCollection = new ServiceCollection();
            //Extension Tag
            //可以将 ClientKey 直接放到这个地方来写, 这样的话下面就不需要写了
            serviceCollection.AddHttpClient(ClientKey);
            
            return ConfigurationClient(serviceCollection);
        }

        /// <summary>
        /// Create HttpClient
        /// </summary>
        /// <param name="ClientKey">HttpClient Key</param>
        /// <param name="configureClient">Configure the HttpClient accordingly</param>
        /// <returns></returns>
        public static HttpClient CreateHttpClient(string ClientKey, Action<HttpClient> ConfigureClient)
        {
            //创建 服务集合
            ServiceCollection serviceCollection = new ServiceCollection();
            //Extension Tag
            //可以将 ClientKey 直接放到这个地方来写, 这样的话下面就不需要写了
            //可以就在这里指定固定 url,默认报文头名字等其他
            //serviceCollection.AddHttpClient(ClientKey, (client) =>
            //{
            //    client.BaseAddress = new Uri("https://www.baidu.com");
            //    client.DefaultRequestHeaders.Add("test", "pmx");
            //});
            serviceCollection.AddHttpClient(ClientKey, ConfigureClient);
            
            return ConfigurationClient(serviceCollection);
        }

        private static HttpClient ConfigurationClient(ServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IHttpFactory, HttpFactory>();

            //创建 ServiceProvider
            using ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            using IServiceScope sc = serviceProvider.CreateScope();
            IHttpFactory httpFactory = sc.ServiceProvider.GetRequiredService<IHttpFactory>();
            //返回工厂创建的 HttpClient
            return httpFactory.CreateHttpClient();
        }
    }
}
