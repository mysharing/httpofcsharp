﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.HTTP.HttpClientEx
{
    public class HttpClientHelper
    {
        //HttpClient类旨在提供一个用户入口，其内部管理着不同服务器的TCP连接池
        //所以，当我们需要发起http请求时，最好使用全局单例的HttpClient，而不是每次都new一个HttpClient。
        //HttpClient有预热机制，第一次进行访问时比较慢，所以不应该用到HttpClient就new一个出来，应该使用单例或其他方式获取HttpClient的实例
        /*
        对于新的 windows 系统来说，可能不会有这种情况，但是对于老的 windows 系统，还是需要去注意

        如果 new 一个新的 HttpClient 可能每次会占用不同的端口，如果需要创建多个 HttpClient 可能会发生端口耗尽的情况，所以不推荐
    使用 using HttpClient hc = new HttpClient();
        并且 HttpClient 使用一个端口时这个端口可能不会短时间内被释放掉，就会出现响应的问题
        一般来讲：小型项目可以去这样使用去每次调用时新 new 一个，但是如果对于大型项目或者很多次调用这个 HttpClient 的情况下， HttpClient 的管理非常重要

        解决方案：创建 HttpClient 工厂
        */
        //另外，由于TCP本身在断开连接的时候需要4次挥手动作，而其中又有一个等待时间，所以我们即使将HttpClient.Dispose() 掉也会造成这个TCP连接短时间内无法断开（最长要持续4分钟），如果遇到高并发的话，很可能端口就不够用了。

        
        private HttpClient HClient { get; set; }


        //构造器
        public HttpClientHelper()
        {
            //最好不要每次使用都 using 
            //using HttpClient httpClient = new HttpClient();
            HClient = HttpClientFactoryHelper.CreateHttpClient();
        }

        /// <summary>
        /// Create HttpClient
        /// </summary>
        /// <param name="ClientKey"></param>
        /// <returns></returns>
        public HttpClientHelper(string clientKey)
        {
            HClient = HttpClientFactoryHelper.CreateHttpClient(clientKey);
        }

        /// <summary>
        /// Create HttpClient
        /// </summary>
        /// <param name="ClientKey">HttpClient Key</param>
        /// <param name="configureClient">Configure the HttpClient accordingly</param>
        /// <returns></returns>
        public HttpClientHelper(string clientKey, Action<HttpClient> configureClient)
        {
            HClient = HttpClientFactoryHelper.CreateHttpClient(clientKey, configureClient);
        }

        /// <summary>
        /// Cancel all pending requests for that instance
        /// </summary>
        public void CancelAllPendingRequests()
        {
            HClient.CancelPendingRequests();
        }

        //encapsulates one first, and the others are used later to continue optimization
        /// <summary>
        /// Converts the data to a string type and return
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<string?> GetDataReadAsStringAsync(string uri)
        {
            string? data = null;
            try
            {
                Uri getUri = new Uri(uri);
                HttpResponseMessage httpResponseMessage =  await HClient.GetAsync(getUri);
                //If the request is successful, the data is fetched
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    data = await httpResponseMessage.Content.ReadAsStringAsync();
                }
                return data;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

            
        }

        //encapsulates one first, and the others are used later to continue optimization
        /// <summary>
        /// Converts the data to a string type and return
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<string?> GetDataReadAsStringAsync(string uri, CancellationToken cancelationToken = default)
        {
            string? data = null;
            try
            {
                Uri getUri = new Uri(uri);
                HttpResponseMessage httpResponseMessage = await HClient.GetAsync(getUri, cancelationToken);
                //If the request is successful, the data is fetched
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    data = await httpResponseMessage.Content.ReadAsStringAsync(cancelationToken);
                    //取消就返回 null;
                    if (cancelationToken.IsCancellationRequested)
                    {
                        data = null;
                    }
                }
                return data;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="savePath">文件地址和名字</param>
        /// <param name="cancelationToken">监视取消请求的标记， 默认值赋值为 null</param>
        /// <returns></returns>
        public async Task<bool> GetDownloadFileAsync(string uri, string savePathAndFileName)
        {
            try
            {
                Uri getUri = new Uri(uri);
                /*
                
                HttpCompletionOption enum
                Indicates whether the HttpClient operation is considered complete immediately when the response is available, or after reading the entire answer information containing the context
                
                The operation should complete after reading the entire response including the
                content.
                    ResponseContentRead = 0,

                The operation should complete as soon as a response is available and headers
                are read. The content is not read yet.
                    ResponseHeadersRead = 1
                 * */
                HttpResponseMessage httpResponseMessage = await HClient.GetAsync(getUri, HttpCompletionOption.ResponseHeadersRead);
                //If the request is fail, return false
                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    return false;
                }
                //Gets the length of the content returned by the request
                long? contentLength = httpResponseMessage.Content.Headers.ContentLength;
                //parm 1: path
                //parm 2: the mode of the open file
                //parm 3: method of access to the file 
                //parm 4；Used to control the type of access that other users can have to the same file


                //using FileStream fileStream = File.Open(savePathAndFileName, FileMode.Create, FileAccess.Read, FileShare.Read);
                /*
                上面那样写会报：System.ArgumentException:“Combining FileMode: Create with FileAccess: Read is invalid. Arg_ParamName_Name”
                这个错误是由于在使用 C# 中的 FileStream 类时，同时指定了 FileMode.Create 和 FileAccess.Read 两个参数，这是不合法的。因为 FileMode.Create 表示创建一个新文件或覆盖现有文件，而 FileAccess.Read 表示只能读取文件而不能写入或修改文件。
                要解决这个问题，可以将 FileAccess 参数改为其他值，如 FileAccess.Write 或 FileAccess.ReadWrite。
                 * */
                using FileStream fileStream = File.Open(savePathAndFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                // Create a buffer(缓冲区) with a size of 64KB
                byte[] buffer = new byte[65536];
                // get the stream of response 
                Stream contentStream = await httpResponseMessage.Content.ReadAsStreamAsync();
                // Defines a variable that records the number of bytes per read
                int readLength = 0;
                // The loop asynchronously reads the contents of the response stream until the read is complete
                do
                {
                    /*     return : A task that represents the asynchronous read operation. The value of the TResult
                         parameter contains the total number of bytes read into the buffer. The result
                         value can be less than the number of bytes requested if the number of bytes currently
                         available is less than the requested number, or it can be 0 (zero) if the end
                         of the stream has been reached. */

                    //CancellationToken: 监视取消请求的标记。 默认值为 None

                    readLength = await contentStream.ReadAsync(buffer, 0, buffer.Length);
                    //Write the read content to the file stream
                    await fileStream.WriteAsync(buffer, 0, readLength);

                } while (readLength > 0);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="savePath">文件地址和名字</param>
        /// <param name="cancelationToken">监视取消请求的标记， 默认值赋值为 null</param>
        /// <returns></returns>
        public async Task<bool> GetDownloadFileAsync(string uri, string savePathAndFileName, CancellationToken cancelationToken = default)
        {
            try
            {
                Uri getUri = new Uri(uri);
                /*
                
                HttpCompletionOption enum
                Indicates whether the HttpClient operation is considered complete immediately when the response is available, or after reading the entire answer information containing the context
                
                The operation should complete after reading the entire response including the
                content.
                    ResponseContentRead = 0,

                The operation should complete as soon as a response is available and headers
                are read. The content is not read yet.
                    ResponseHeadersRead = 1
                 * */
                HttpResponseMessage httpResponseMessage = await HClient.GetAsync(getUri, HttpCompletionOption.ResponseHeadersRead);
                //If the request is fail, return false
                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    return false;
                }
                //Gets the length of the content returned by the request
                long? contentLength = httpResponseMessage.Content.Headers.ContentLength;
                //parm 1: path
                //parm 2: the mode of the open file
                //parm 3: method of access to the file 
                //parm 4；Used to control the type of access that other users can have to the same file


                //using FileStream fileStream = File.Open(savePathAndFileName, FileMode.Create, FileAccess.Read, FileShare.Read);
                /*
                上面那样写会报：System.ArgumentException:“Combining FileMode: Create with FileAccess: Read is invalid. Arg_ParamName_Name”
                这个错误是由于在使用 C# 中的 FileStream 类时，同时指定了 FileMode.Create 和 FileAccess.Read 两个参数，这是不合法的。因为 FileMode.Create 表示创建一个新文件或覆盖现有文件，而 FileAccess.Read 表示只能读取文件而不能写入或修改文件。
                要解决这个问题，可以将 FileAccess 参数改为其他值，如 FileAccess.Write 或 FileAccess.ReadWrite。
                 * */
                using FileStream fileStream = File.Open(savePathAndFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
                // Create a buffer(缓冲区) with a size of 64KB
                byte[] buffer = new byte[65536];
                // get the stream of response 
                Stream contentStream = await httpResponseMessage.Content.ReadAsStreamAsync();
                // Defines a variable that records the number of bytes per read
                int readLength = 0;
                // The loop asynchronously reads the contents of the response stream until the read is complete
                do
                {
                    /*     return : A task that represents the asynchronous read operation. The value of the TResult
                         parameter contains the total number of bytes read into the buffer. The result
                         value can be less than the number of bytes requested if the number of bytes currently
                         available is less than the requested number, or it can be 0 (zero) if the end
                         of the stream has been reached. */

                    //CancellationToken: 监视取消请求的标记。 默认值为 None

                    readLength = await contentStream.ReadAsync(buffer, 0, buffer.Length, cancelationToken);
                    if (cancelationToken.IsCancellationRequested)
                    {
                        fileStream.Close();
                        File.Delete(savePathAndFileName);
                        return false;
                    }
                    //Write the read content to the file stream
                    await fileStream.WriteAsync(buffer, 0, readLength, cancelationToken);

                } while (readLength > 0);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return true;
        }

        /// <summary>
        /// 发送 Json 数据并获取返回值,异常返回 null
        /// </summary>
        /// <param name="uri">地址</param>
        /// <param name="jsonData">Json 数据</param>
        /// <param name="ContentTypeEnum">内容类型 </param>
        /// <returns></returns>
        public async Task<string?> PostJsonDataAsync(string uri, string jsonData, ContentTypeEnum ContentTypeEnum = ContentTypeEnum.Application_Json)
        {
            string? responseMessage = null;
            try
            {
                
                if (!JsonHelper.IsJson(jsonData))
                {
                    return responseMessage;
                }
                Uri postUri = new Uri(uri);
                //将 Json 数据生成 HttpContent 对象
                HttpContent? httpContent = new StringContent(jsonData);
                //指定头文件内容类型
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentTypeEnum.ContentTypeConvrt());

                HttpResponseMessage httpResponseMessage = await HClient.PostAsync(postUri, httpContent);
                //判断是否获取成功
                if (httpResponseMessage.IsSuccessStatusCode) 
                {
                    //获取返回值
                    responseMessage = await httpResponseMessage.Content.ReadAsStringAsync();
                }
                
                return responseMessage;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// 发送 Json 数据并获取返回值,异常返回 null
        /// </summary>
        /// <param name="uri">地址</param>
        /// <param name="jsonData">Json 数据</param>
        /// <param name="ContentTypeEnum">内容类型 </param>
        /// <returns></returns>
        public async Task<string?> PostJsonDataAsync(string uri, string jsonData, ContentTypeEnum ContentTypeEnum = ContentTypeEnum.Application_Json, CancellationToken cancellationToken = default)
        {
            string? responseMessage = null;
            try
            {

                if (!JsonHelper.IsJson(jsonData))
                {
                    return responseMessage;
                }
                Uri postUri = new Uri(uri);
                //将 Json 数据生成 HttpContent 对象
                HttpContent? httpContent = new StringContent(jsonData);
                //指定头文件内容类型
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentTypeEnum.ContentTypeConvrt());

                HttpResponseMessage httpResponseMessage = await HClient.PostAsync(postUri, httpContent, cancellationToken);

               if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //获取返回值
                    responseMessage = await httpResponseMessage.Content.ReadAsStringAsync(cancellationToken);
                    if(cancellationToken.IsCancellationRequested)
                    {
                        responseMessage = null;
                    }
                }

                return responseMessage;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        /// 发送 Json 数据并获取返回值,异常返回 null
        /// </summary>
        /// <param name="uri">地址</param>
        /// <param name="jsonData">Json 数据</param>
        /// <param name="ContentTypeEnum">内容类型 </param>
        /// <returns></returns>
        public async Task<string?> PostDataAsync(string uri, string Data, ContentTypeEnum ContentTypeEnum = ContentTypeEnum.Application_Json)
        {
            string? responseMessage = null;
            try
            {
                Uri postUri = new Uri(uri);
                //将  数据生成 HttpContent 对象
                HttpContent? httpContent = new StringContent(Data);
                //指定头文件内容类型
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentTypeEnum.ContentTypeConvrt());

                HttpResponseMessage httpResponseMessage = await HClient.PostAsync(postUri, httpContent);
                //判断是否获取成功
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //获取返回值
                    responseMessage = await httpResponseMessage.Content.ReadAsStringAsync();
                }

                return responseMessage;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// 上传文件内容
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> UploadFilesAsync(string uri, string path, string watermark, ContentTypeEnum ContentTypeEnum = ContentTypeEnum.Application_Json)
        {
            try
            {
                //对于地址拼接，第一种方式
                //uri += $"?watermark={watermark}";
                //使用 UriBuilder
                UriBuilder uriBuilder = new UriBuilder(uri);
                uriBuilder.Query = $"watermark={watermark}";
                Uri fileUri = uriBuilder.Uri;
                //读取文件字节
                using FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);

                HttpContent fileContent = new StreamContent(fileStream);
                fileContent.Headers.ContentType = new MediaTypeHeaderValue(ContentTypeEnum.ContentTypeConvrt());

                //MultipartFormDataContent类是一个专门用于存放multipart表单数据的类，通过其Add方法可以将表单数据加进去。
                using MultipartFormDataContent formData = new MultipartFormDataContent();

                //如果是文件的话，一定要将文件名加上，才能在 Controller Action 方法里 [FromForm]调用到
                formData.Add(fileContent, "File", Path.GetFileName(path));
                formData.Add(new StringContent("test"), "Test");

                HttpResponseMessage httpResponse = await HClient.PostAsync(fileUri, formData);
                
                return httpResponse;
            }
            catch(Exception e)
            {
                throw new Exception (e.Message);
            }
        }

    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
//CancelPendingRequests()
//取消该实例所有挂起的请求。

//DeleteAsync(String)
//以异步操作将 DELETE 请求发送给指定 URI。

//DeleteAsync(String, CancellationToken)
//用以异步操作的取消标记发送 DELETE 请求到指定的 URI。

//DeleteAsync(Uri)
//以异步操作将 DELETE 请求发送给指定 URI。

//DeleteAsync(Uri, CancellationToken)
//用以异步操作的取消标记发送 DELETE 请求到指定的 URI。

//Dispose()
//释放由 HttpMessageInvoker 使用的非托管资源和托管资源。

//(继承自 HttpMessageInvoker)
//Dispose(Boolean)
//释放由 HttpClient 使用的非托管资源，并可根据需要释放托管资源。

//Equals(Object)
//确定指定对象是否等于当前对象。

//(继承自 Object)
//GetAsync(String)
//以异步操作将 GET 请求发送给指定 URI。

//GetAsync(String, CancellationToken)
//用以异步操作的取消标记发送 GET 请求到指定的 URI。

//GetAsync(String, HttpCompletionOption)
//用以异步操作的 HTTP 完成选项发送 GET 请求到指定的 URI。

//GetAsync(String, HttpCompletionOption, CancellationToken)
//用以异步操作的 HTTP 完成选项和取消标记发送 GET 请求到指定的 URI。

//GetAsync(Uri)
//以异步操作将 GET 请求发送给指定 URI。

//GetAsync(Uri, CancellationToken)
//用以异步操作的取消标记发送 GET 请求到指定的 URI。

//GetAsync(Uri, HttpCompletionOption)
//用以异步操作的 HTTP 完成选项发送 GET 请求到指定的 URI。

//GetAsync(Uri, HttpCompletionOption, CancellationToken)
//用以异步操作的 HTTP 完成选项和取消标记发送 GET 请求到指定的 URI。

//GetByteArrayAsync(String)
//将 GET 请求发送到指定 URI 并在异步操作中以字节数组的形式返回响应正文。

//GetByteArrayAsync(String, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以字节数组的形式返回响应正文。

//GetByteArrayAsync(Uri)
//将 GET 请求发送到指定 URI 并在异步操作中以字节数组的形式返回响应正文。

//GetByteArrayAsync(Uri, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以字节数组的形式返回响应正文。

//GetHashCode()
//作为默认哈希函数。

//(继承自 Object)
//GetStreamAsync(String)
//将 GET 请求发送到指定 URI 并在异步操作中以流的形式返回响应正文。

//GetStreamAsync(String, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以流的形式返回响应正文。

//GetStreamAsync(Uri)
//将 GET 请求发送到指定 URI 并在异步操作中以流的形式返回响应正文。

//GetStreamAsync(Uri, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以流的形式返回响应正文。

//GetStringAsync(String)
//将 GET 请求发送到指定 URI 并在异步操作中以字符串的形式返回响应正文。

//GetStringAsync(String, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以字符串的形式返回响应正文。

//GetStringAsync(Uri)
//将 GET 请求发送到指定 URI 并在异步操作中以字符串的形式返回响应正文。

//GetStringAsync(Uri, CancellationToken)
//将 GET 请求发送到指定 URI 并在异步操作中以字符串的形式返回响应正文。

//GetType()
//获取当前实例的 Type。

//(继承自 Object)
//MemberwiseClone()
//创建当前 Object 的浅表副本。

//(继承自 Object)
//PatchAsync(String, HttpContent)
//以异步操作方式将 PATCH 请求发送到指定为字符串的 URI。

//PatchAsync(String, HttpContent, CancellationToken)
//以异步操作方式将带有取消令牌的 PATCH 请求发送到表示为字符串的 URI。

//PatchAsync(Uri, HttpContent)
//以异步操作方式发送 PATCH 请求。

//PatchAsync(Uri, HttpContent, CancellationToken)
//以异步操作方式发送带有取消令牌的 PATCH 请求。

//PostAsync(String, HttpContent)
//以异步操作将 POST 请求发送给指定 URI。

//PostAsync(String, HttpContent, CancellationToken)
//用以异步操作的取消标记发送 POST 请求。

//PostAsync(Uri, HttpContent)
//以异步操作将 POST 请求发送给指定 URI。

//PostAsync(Uri, HttpContent, CancellationToken)
//用以异步操作的取消标记发送 POST 请求。

//PutAsync(String, HttpContent)
//以异步操作将 PUT 请求发送给指定 URI。

//PutAsync(String, HttpContent, CancellationToken)
//用以异步操作的取消标记发送 PUT 请求。

//PutAsync(Uri, HttpContent)
//以异步操作将 PUT 请求发送给指定 URI。

//PutAsync(Uri, HttpContent, CancellationToken)
//用以异步操作的取消标记发送 PUT 请求。

//Send(HttpRequestMessage)
//发送带有指定请求的 HTTP 请求。

//Send(HttpRequestMessage, CancellationToken)
//发送带有指定请求和取消标记的 HTTP 请求。

//Send(HttpRequestMessage, HttpCompletionOption)
//发送 HTTP 请求。

//Send(HttpRequestMessage, HttpCompletionOption, CancellationToken)
//发送带有指定请求、完成选项和取消标记的 HTTP 请求。

//SendAsync(HttpRequestMessage)
//以异步操作发送 HTTP 请求。

//SendAsync(HttpRequestMessage, CancellationToken)
//以异步操作发送 HTTP 请求。

//SendAsync(HttpRequestMessage, HttpCompletionOption)
//以异步操作发送 HTTP 请求。

//SendAsync(HttpRequestMessage, HttpCompletionOption, CancellationToken)
//以异步操作发送 HTTP 请求。

//ToString()
//返回表示当前对象的字符串。

//(继承自 Object)
//扩展方法
//DeleteFromJsonAsync(HttpClient, String, Type, JsonSerializerOptions, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync(HttpClient, String, Type, JsonSerializerContext, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync(HttpClient, String, Type, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync(HttpClient, Uri, Type, JsonSerializerOptions, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync(HttpClient, Uri, Type, JsonSerializerContext, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync(HttpClient, Uri, Type, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, String, JsonSerializerOptions, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, String, JsonTypeInfo<TValue>, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, String, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, Uri, JsonSerializerOptions, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, Uri, JsonTypeInfo<TValue>, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//DeleteFromJsonAsync<TValue>(HttpClient, Uri, CancellationToken)
//将 DELETE 请求发送到指定的 URI，并返回在异步操作中将响应正文反序列化为 JSON 而得出的值。

//GetFromJsonAsync(HttpClient, String, Type, JsonSerializerOptions, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync(HttpClient, String, Type, JsonSerializerContext, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync(HttpClient, String, Type, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync(HttpClient, Uri, Type, JsonSerializerOptions, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync(HttpClient, Uri, Type, JsonSerializerContext, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync(HttpClient, Uri, Type, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, String, JsonSerializerOptions, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, String, JsonTypeInfo<TValue>, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, String, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, Uri, JsonSerializerOptions, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, Uri, JsonTypeInfo<TValue>, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//GetFromJsonAsync<TValue>(HttpClient, Uri, CancellationToken)
//将 GET 请求发送到指定 URI，并在异步操作中以 JSON 形式返回反序列化响应正文生成的值。

//PatchAsJsonAsync<TValue>(HttpClient, String, TValue, JsonSerializerOptions, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PatchAsJsonAsync<TValue>(HttpClient, String, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PatchAsJsonAsync<TValue>(HttpClient, String, TValue, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PatchAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonSerializerOptions, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PatchAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PatchAsJsonAsync<TValue>(HttpClient, Uri, TValue, CancellationToken)
//将 PATCH 请求发送到在请求正文中包含序列化为 JSON 的指定 URI value 。

//PostAsJsonAsync<TValue>(HttpClient, String, TValue, JsonSerializerOptions, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PostAsJsonAsync<TValue>(HttpClient, String, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PostAsJsonAsync<TValue>(HttpClient, String, TValue, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PostAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonSerializerOptions, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PostAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PostAsJsonAsync<TValue>(HttpClient, Uri, TValue, CancellationToken)
//将 POST 请求发送到指定 URI，该 URI 包含请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, String, TValue, JsonSerializerOptions, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, String, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, String, TValue, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonSerializerOptions, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, Uri, TValue, JsonTypeInfo<TValue>, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。

//PutAsJsonAsync<TValue>(HttpClient, Uri, TValue, CancellationToken)
//将 PUT 请求发送到指定 URI，该 URI 包含在请求正文中序列化为 JSON 的 value。