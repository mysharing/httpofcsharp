﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.HTTP.HttpClientEx
{
    //可以通过重写 HttpClient 的方法来魔改 HttpClient
    //重写一下 HttpClient 对其内部可以重写的方法进行一个重写,当然我这里只是做一个笔记，并没有修改和调用这个类
    /*
    调用类 HttpMessageHandlerExtension；
    其实魔改还有一个更好的办法， 我们可以看到 HttpClient 的本质其实是 HttpMessageHandler，可以集成他去重写魔改一些可以重写的方法
    不过不是直接继承 HttpMessageHandler, 而是集成官方指定的一个类 DelegatingHandler，这样重新创建一个类，然后重写 对应方法
    然后再将该类依赖注入进去 serviceCollection。AddScpoe<HttpMessageHandlerExtension>()，并且
    再  serviceCollection.AddHttpClient<T>().AddHttpMessageHandler<HttpMessageHandlerExtension>(), 将其也注入进去，
    注意一定是 serviceCollection.AddHttpClient<T>(); 这个方法才行; 这里并没有对这里进行实现，只是方便日后研究
    */
    class HttpClientExtension : HttpClient
    {
        
        public override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return await base.SendAsync(request, cancellationToken);
        }

        public override HttpResponseMessage Send(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return base.Send(request, cancellationToken);
        }
    }
}
