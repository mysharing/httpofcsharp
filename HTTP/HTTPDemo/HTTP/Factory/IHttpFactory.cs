﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.HTTP.Factory
{
    internal interface IHttpFactory
    {
        //创建 HttpClient
        HttpClient CreateHttpClient();
    }
}
