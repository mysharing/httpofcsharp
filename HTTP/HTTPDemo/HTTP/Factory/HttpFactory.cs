﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.HTTP.Factory
{
    internal class HttpFactory : IHttpFactory
    {
        private readonly IHttpClientFactory HttpClientFactory;
        public HttpFactory(IHttpClientFactory httpClientFactory)
        {
            HttpClientFactory = httpClientFactory;
        }

        public HttpClient CreateHttpClient()
        {
            //You can specify a name for HttpClient, otherwise use its default name
            //Call IHttpClientFactory.CreateClient(string name) method, manually specified default value
            //IHttpClientFactory.CreateClient(Options.DefaultName);
            return HttpClientFactory.CreateClient();
        }

    }
}
