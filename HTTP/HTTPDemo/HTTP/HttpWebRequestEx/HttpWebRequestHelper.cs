﻿using System.Net;
using System.Text;

namespace PengMingXing.NetCommunicationCommons.HTTP.HttpWebRequestEx
{
    public static class HttpWebRequestHelper
    {
        /// <summary>
        /// Send and transfer data asynchronously
        /// </summary>
        /// <param name="url">address</param>
        /// <param name="contentType">Content type</param>
        /// <param name="method">Post</param>
        /// <param name="data">data</param>
        /// <param name="timeOut">Timeout period, set the default time</param>
        /// <returns></returns>
        public static async Task<string?> PostDataAndResponseAsync(string url, string data, ContentTypeEnum contentType = ContentTypeEnum.Application_Json, int timeOut = 100000)
        {
            string? result = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "Post";//设置请求方法
                httpWebRequest.Timeout = timeOut;


                httpWebRequest.ContentType = contentType.ContentTypeConvrt();//设置内容类型
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                httpWebRequest.ContentLength = buffer.Length;

                //以流的形式附加参数
                //获取用于写入请求数据的流对象
                using Stream requestStream = httpWebRequest.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);//写入流
                requestStream.Close();

                //获取返回结果，如果超时，会报错
                result = await ResponseByReadToEndAsync(httpWebRequest);
            }
            catch
            {
                return result;
            }
            return result;
        }

        /// <summary>
        /// Send and transfer data
        /// </summary>
        /// <param name="url">address</param>
        /// <param name="contentType">Content type</param>
        /// <param name="method">Post</param>
        /// <param name="data">data</param>
        /// <param name="timeOut">Timeout period, set the default time</param>
        /// <returns></returns>
        public static string? PostDataAndResponse(string url, string data, ContentTypeEnum contentType = ContentTypeEnum.Application_Json, int timeOut = 100000)
        {
            string? result = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "Post";//设置请求方法
                httpWebRequest.Timeout = timeOut;


                httpWebRequest.ContentType = contentType.ContentTypeConvrt();//设置内容类型
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                httpWebRequest.ContentLength = buffer.Length;

                //以流的形式附加参数
                //获取用于写入请求数据的流对象
                using Stream requestStream = httpWebRequest.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);//写入流
                requestStream.Close();

                //获取返回结果，如果超时，会报错
                result = ResponseByReadToEnd(httpWebRequest);


            }
            catch
            {
                return result;
            }
            return result;
        }

        /// <summary>
        /// Get data asynchronously
        /// </summary>
        /// <param name="url">address</param>
        /// <param name="timeOut">Timeout period, set the default time</param>
        /// <returns></returns>
        public static async Task<string?> GetResponseDataAsync(string url, ContentTypeEnum contentType = ContentTypeEnum.Application_Json, int timeOut = 100000)
        {
            string? result = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "GET";//设置请求方法
                httpWebRequest.Timeout = timeOut;
                httpWebRequest.ContentType = contentType.ContentTypeConvrt();//设置内容类型


                result = await ResponseByReadToEndAsync(httpWebRequest);

            }
            catch
            {
                return result;
            }
            return result;
        }

        /// <summary>
        /// Get data
        /// </summary>
        /// <param name="url">address</param>
        /// <param name="timeOut">Timeout period, set the default time</param>
        public static string? GetResponseData(string url, ContentTypeEnum contentType = ContentTypeEnum.Application_Json, int timeOut = 100000)
        {
            string? result = null;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "GET";//设置请求方法
                httpWebRequest.Timeout = timeOut;
                httpWebRequest.ContentType = contentType.ContentTypeConvrt();//设置内容类型

                result = ResponseByReadToEnd(httpWebRequest);

            }
            catch
            {
                return result;
            }
            return result;
        }

        private static string? ResponseByReadToEnd(HttpWebRequest httpWebRequest)
        {
            string? result = null;
            try
            {
                using HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();//获取到数据
                using Stream responseStream = httpWebResponse.GetResponseStream();//将数据转化为流
                using StreamReader streamReader = new StreamReader(responseStream);//将 Stream 转换为 StreamReader，使我们可以以一种编码方式从流中读取数据
                result = streamReader.ReadToEnd();

                httpWebResponse.Close();
                responseStream.Close();
                streamReader.Close();
            }
            catch
            {
                return result;
            }


            return result;
        }

        private static async Task<string?> ResponseByReadToEndAsync(HttpWebRequest httpWebRequest)
        {
            string? result = null;
            try
            {
                using HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();//获取到数据
                using Stream responseStream = httpWebResponse.GetResponseStream();//将数据转化为流
                using StreamReader streamReader = new StreamReader(responseStream);//将 Stream 转换为 StreamReader，使我们可以以一种编码方式从流中读取数据
                result = await streamReader.ReadToEndAsync();

                httpWebResponse.Close();
                responseStream.Close();
                streamReader.Close();
            }
            catch
            {
                return result;
            }


            return result;
        }

    }

}