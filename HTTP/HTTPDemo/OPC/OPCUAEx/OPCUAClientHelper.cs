﻿using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Asn1.X509.Qualified;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace PengMingXing.NetCommunicationCommons.OPC.OPCUAEx
{
    
    public class OPCUAClientHelper
    {

        private OPCUAClient OPCClient { get; set; }// OPCUA 基类
        private bool ContinuousAccessToData { get; set; }

        private DataTable ReceiveDTable { get; set; }

        private Task ReceiveTagTask { get; set; }

        //每当获取数据就调用该事件
        public delegate void OnTagValueReceiveDelegate(DataRow dr);
        public event OnTagValueReceiveDelegate OnTagValueReceiveEvent;

        public OPCUAClientHelper(string ServerUrl)
        {
            OPCClient = new OPCUAClient(ServerUrl);
            ContinuousAccessToData = true;
            ReceiveDTable = new DataTable();
        }

        public OPCUAClientHelper(string ServerUrl, string User, string Password)
        {
            OPCClient = new OPCUAClient(ServerUrl, User, Password);
            ContinuousAccessToData = true;
            ReceiveDTable = new DataTable();   
        }

        public OPCUAClientHelper(string ServerUrl, string CertificationPath, string Key, X509KeyStorageFlags keyStorageFlags = default)
        {
            OPCClient = new OPCUAClient(ServerUrl, CertificationPath, Key, keyStorageFlags = default);
            ContinuousAccessToData = true;
            ReceiveDTable = new DataTable();
        }

        public async Task<bool> ConnectServer(Dictionary<string, Type> keyValues)
        {
            ContinuousAccessToData = true;
            bool connect = await OPCClient.ConnectServer();
            try
            {
                if (connect)
                {
                    if (keyValues == null || keyValues.Count <= 0)
                    {
                        OPCClient.DisConnectServer();
                        return false;
                    }
                    ReceiveTagTask = Task.Run(async () =>
                    {
                        GetValueByDic(keyValues);
                    }); 
                }
            }
            catch (Exception ex)
            {
                if (connect)
                {
                    OPCClient.DisConnectServer();
                }
                return false;
            }           
            return connect;
        }


        public void DisConnectServer()
        {
            ContinuousAccessToData = false;
            ReceiveTagTask.Dispose();//销毁先异步
            OPCClient.DisConnectServer();

        }

        private async Task GetValueByDic(Dictionary<string, Type> keyValues)
        {
            try
            {
                if (keyValues == null || keyValues.Count <= 0)
                {
                    throw new Exception("获取节点失败");
                }
                CreateDataTable(keyValues);
                while (ContinuousAccessToData)
                {
                    if (ReceiveDTable == null || ReceiveDTable.Columns.Count == 0)
                    {
                        throw new Exception("创建 Datatable 失败");
                    }
                    //如果没有登录上，即跳过
                    if (!OPCClient.ConnectStatus)
                    {
                        throw new Exception("OPC服务端未连接！");
                    }
                    DataRow dr = ReceiveDTable.NewRow();
                    if (!(ReceiveDTable.Rows.Count == 0))
                    {
                        ReceiveDTable.Rows.Clear();//先清空之前的数据
                    }


                    foreach (KeyValuePair<string, Type> item in keyValues)
                    {
                        if (item.Value == typeof(int))
                        {
                            int value = await setDicValue<int>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(Int16))
                        {
                            Int16 value = await setDicValue<Int16>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(Int32))
                        {
                            Int32 value = await setDicValue<Int32>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(Int64))
                        {
                            Int64 value = await setDicValue<Int64>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(string))
                        {
                            string value = await setDicValue<string>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(byte))
                        {
                            byte value = await setDicValue<byte>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(double))
                        {
                            double value = await setDicValue<double>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(float))
                        {
                            float value = await setDicValue<float>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(char))
                        {
                            char value = await setDicValue<char>(item.Key);
                            dr[item.Key] = value;
                        }
                        else if (item.Value == typeof(bool))
                        {
                            bool value = await setDicValue<bool>(item.Key);
                            dr[item.Key] = value;
                        }
                    }
                    ReceiveDTable.Rows.Add(dr);
                    if (ReceiveDTable.Rows.Count > 0)
                    {
                        OnTagValueReceiveEvent(ReceiveDTable.Rows[0]);
                    }  
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        private async Task<T> setDicValue<T>(string tag) 
        {
            return await OPCClient.GetServerNodeValueAsync<T>(tag);
            
        }

        private void CreateDataTable(Dictionary<string, Type> keyValues)
        {
            foreach (KeyValuePair<string, Type> item in keyValues)
            {
                //string key = item.Key.Replace("=", "").Replace(".", "");
                ReceiveDTable.Columns.Add(item.Key, item.Value);
            }
        }

        /// <summary>
        /// 写入单个节点的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetServerNodeValue<T>(string tag, T value)
        {
            try
            {
                return OPCClient.SetServerNodeValue<T>(tag, value);
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// 写入单个节点的值异步
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<bool> SetServerNodeValueAsync<T>(string tag, T value)
        {
            try
            {
                return await OPCClient.SetServerNodeValueAsync<T>(tag, value);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// 批量写入节点的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetServerNodeValues(string[] tag, object[] value)
        {
            try
            {
                return OPCClient.SetServerNodeValues(tag, value);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
