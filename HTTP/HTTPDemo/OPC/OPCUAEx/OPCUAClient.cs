﻿using Opc.Ua;
using Opc.Ua.Client;
using OpcUaHelper;
using System.ComponentModel;
using System.Data;
using System.Security.Cryptography.X509Certificates;

namespace PengMingXing.NetCommunicationCommons.OPC.OPCUAEx
{
    /// <summary>
    /// 读取KepServer 节点书写方式： "ns=2;s=通道 1.sl.kk"
    /// </summary>
    class OPCUAClient
    {
        [Description("OPC Server Url")]
        public readonly string ServerUrl;
        private OpcUaClient OpcUA;

        [Description("Connection status")]
        public bool ConnectStatus
        {
            get { return null == OpcUA ? false : OpcUA.Connected; }
        }

        #region 构造器
        [Description("Anonymous connection")]
        public OPCUAClient(string ServerUrl)
        {
            this.ServerUrl = ServerUrl;
            //OPC
            OpcUA = new OpcUaClient();
            //设置连接权限，设置匿名连接
            OpcUA.UserIdentity = new UserIdentity(new AnonymousIdentityToken());
        }

        [Description("Set the username and password for the connection")]
        public OPCUAClient(string ServerUrl, string User, string Password)
        {
            this.ServerUrl = ServerUrl;
            //OPC
            OpcUA = new OpcUaClient();
            //设置连接权限,设置用户名连接
            OpcUA.UserIdentity = new UserIdentity(User, Password);
        }

        [Description("Certificate login")]
        public OPCUAClient(string ServerUrl, string CertificationPath, string Key, X509KeyStorageFlags keyStorageFlags = default)
        {
            if (keyStorageFlags == default)
            {
                keyStorageFlags = X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable;
            }
            this.ServerUrl = ServerUrl;
            //OPC
            OpcUA = new OpcUaClient();
            //设置连接权限
            X509Certificate2 certificate = new X509Certificate2(CertificationPath, Key, keyStorageFlags);
            OpcUA.UserIdentity = new UserIdentity(certificate);
        }

        #endregion


        [Description("Connect the service")]
        public virtual async Task<bool> ConnectServer(Dictionary<string, Type>? keyValues = null)
        {
            try
            {
                await OpcUA.ConnectServer(ServerUrl);
                //如果连接上服务即可进行下一步操作
                if (ConnectStatus)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual void DisConnectServer()
        {
            try
            {
                if (ConnectStatus)
                {
                    OpcUA.Disconnect();
                }
            }
            catch
            {
                Console.WriteLine("断开连接失败");
            }
        }
        /// <summary>
        ///获取到当前节点值
        /// </summary>
        public T GetServerNodeValue<T>(string tag)
        {
            T value = default;
            if (tag != null && ConnectStatus)
            {
                try
                {
                    value = OpcUA.ReadNode<T>(tag);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return value;
        }

        /// <summary>
        ///获取到当前根节点
        /// </summary>
        public DataValue GetServerNodeValue(string tag)
        {
            DataValue value = default;
            if (tag != null && ConnectStatus)
            {
                try
                {
                    value = OpcUA.ReadNode(tag);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return value;
        }


        /// <summary>
        ///获取到多个节点值节点值
        /// </summary>
        public Dictionary<string, DataValue> GetServerNodeValues(List<NodeId> nodeIds)
        {
            Dictionary<string, DataValue> dicDataValue = new Dictionary<string, DataValue>();
            if (nodeIds != null && nodeIds.Count > 0 && ConnectStatus)
            {
                try
                {
                    List<DataValue> listDV = OpcUA.ReadNodes(nodeIds.ToArray());
                    for (int i = 0; i < nodeIds.Count; i++)
                    {
                        dicDataValue.Add(nodeIds[i].ToString(), listDV[i]);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return dicDataValue;
        }
        /// <summary>
        ///获取到当前节点值异步
        /// </summary>
        public async Task<T> GetServerNodeValueAsync<T>(string tag)
        {
            T value = default;
            if (tag != null && ConnectStatus)
            {
                try
                {
                    value = await OpcUA.ReadNodeAsync<T>(tag);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return value;
        }
        /// <summary>
        ///获取到多个节点值节点值异步
        /// </summary>
        public async Task<Dictionary<string, DataValue>> GetServerNodeValuesAsync(List<NodeId> nodeIds)
        {
            Dictionary<string, DataValue> dicDataValue = new Dictionary<string, DataValue>();
            if (nodeIds != null && nodeIds.Count > 0 && ConnectStatus)
            {
                try
                {
                    List<DataValue> listDV = await OpcUA.ReadNodesAsync(nodeIds.ToArray());
                    for (int i = 0; i < nodeIds.Count; i++)
                    {
                        dicDataValue.Add(nodeIds[i].ToString(), listDV[i]);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return dicDataValue;
        }

        /// <summary>
        /// 写入单个节点的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual bool SetServerNodeValue<T>(string tag, T value)
        {
            bool isSetIn = false;
            if (tag != null && ConnectStatus)
            {
                try
                {
                    isSetIn = OpcUA.WriteNode(tag, value);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return isSetIn;
        }

        /// <summary>
        /// 写入单个节点的值异步
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual async Task<bool> SetServerNodeValueAsync<T>(string tag, T value)
        {
            bool isSetIn = false;
            if (tag != null && ConnectStatus)
            {
                try
                {
                    isSetIn = await OpcUA.WriteNodeAsync(tag, value);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return isSetIn;
        }

        /// <summary>
        /// 批量写入节点的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual bool SetServerNodeValues(string[] tag, object[] value)
        {
            bool isSetIn = false;
            if (tag != null && tag.Count() > 0 && ConnectStatus)
            {
                try
                {
                    isSetIn = OpcUA.WriteNodes(tag, value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return isSetIn;
        }


        /// <summary>
        /// 单节点数据订阅
        /// </summary>
        /// <param name="key">订阅的关键字（必须唯一）</param>
        /// <param name="nodeId">节点</param>
        /// <param name="callback">数据订阅的回调方法</param>
        public void SingleNodeIdSubscription(string key, string nodeId, Action<string, MonitoredItem, MonitoredItemNotificationEventArgs> callback)
        {
            if (ConnectStatus)
            {
                try
                {
                    OpcUA.AddSubscription(key, nodeId, callback);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// 取消单节点数据订阅
        /// </summary>
        /// <param name="key">订阅的关键字</param>
        public bool CancelSingleNodeIdSubscription(string key)
        {
            bool isSingle = false;
            if (!string.IsNullOrEmpty(key))
            {
                if (ConnectStatus)
                {
                    try
                    {
                        OpcUA.RemoveSubscription(key);
                        isSingle = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
            }
            return isSingle;
        }


        /// <summary>
        /// 批量节点数据订阅
        /// </summary>
        /// <param name="key">订阅的关键字（必须唯一）</param>
        /// <param name="nodeIds">节点数组</param>
        /// <param name="callback">数据订阅的回调方法</param>
        public void BatchNodeIdSubscription(string key, string[] nodeIds, Action<string, MonitoredItem, MonitoredItemNotificationEventArgs> callback)
        {
            if (!string.IsNullOrEmpty(key) && nodeIds != null && nodeIds.Length > 0)
            {
                if (ConnectStatus)
                {
                    try
                    {
                        OpcUA.AddSubscription(key, nodeIds, callback);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

        }

        /// <summary>
        /// 取消所有节点的数据订阅
        /// </summary>
        /// <returns></returns>
        public bool CancelAllNodeIdDatasSubscription()
        {
            bool IsSingle = false;

            if (ConnectStatus)
            {
                try
                {
                    OpcUA.RemoveAllSubscription();
                    IsSingle = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }

            return IsSingle;
        }

    }
}

